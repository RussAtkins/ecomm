# README

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

- Quick summary

Product page tech test

### How do I get set up?

- Summary of set up
    1. `npm i`
    2. `npm run live`
    3. `npm start`
- Configuration
    - created on node 10
- Dependencies
    - noted in package.json
- Database configuration
    - N/A
- Dev instructions
    - `npm run live` is needed for the mock product endpoint
    - `npm start` to start the webpack live reload server 
- How to run tests
    - `npm run test` for a console output
    - `npm run cypress` to see whats going on
    - `npm run coverage` for code coverage
- Deployment instructions
    - `npm run build` if you want to deploy the build folder on s3 or something
    - `npm run live` if you want to serve the app from an express server with a mock json endpoint for the product data

### Who do I talk to?

- russ@russatkins.co.uk
- 07594024299

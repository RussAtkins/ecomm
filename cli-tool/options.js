const { prompt } = require("enquirer");
const fs = require("fs");

const question = [
  {
    type: "input",
    name: "wcname",
    message: "What is the name of the component?",
  },
];

const capitalize = (s) => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

prompt(question)
  .then((answer) => {
    // console.log('Answer:', answer)
    let wcname = capitalize(answer.wcname),
      dir = `./src/components/${wcname}`;

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }

    writefile(dir, "index.js", wcname);
    writefile(dir, "index.scss", wcname);
    writefile(dir, "slice.js", wcname);
  })
  .then(function () {
    console.log(`New component created! Happy componenting!`);
  });

let writefile = (dir, filename, wcname) => {
  console.log(dir, filename, wcname);
  fs.readFile(
    `./src/component-template/${filename}`,
    "utf8",
    function (err, contents) {
      console.log(err, contents);
      fs.writeFile(
        `${dir}/${filename}`,
        contents.replace(`{{wcname}}`, wcname),
        (err) => {
          if (err) throw err;
        }
      );
    }
  );
};

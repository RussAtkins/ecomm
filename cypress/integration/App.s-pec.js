context("Assertions", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");
  });

  describe("App should load", () => {
    it(".should() - be visible", () => {
      cy.get("#root").should("be.visible");
    });
  });
});

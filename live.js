const express = require("express");
const app = express();
const cors = require("cors");
const port = 3001;

app.use(cors());

const mockData = {
  product: {
    title: "Nintendo Switch Console",
    colours: [
      {
        color: "pink",
        image:
          "https://media.very.co.uk/i/very/QE9G4_SQ1_0000000054_CORAL_SLf?$550x733_standard$",
        defaultImage: true,
      },
      {
        color: "grey",
        image:
          "https://media.very.co.uk/i/very/PEM4V_SQ1_0000000005_GREY_SLf?$550x733_standard$",
        defaultImage: false,
      },
    ],
  },
};

app.get("/product/productid1", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify(mockData));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

app.use(express.static("build"));

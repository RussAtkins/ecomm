// import Counter from '../components/Counter';
import Grid from "../components/Grid";
import Griditem from "../components/Griditem";

const Home = () => (
  <div>
    <Grid>
      <Griditem className="a-griditem--full">
        <h2>Welcome</h2>
        {/* <Counter /> */}
      </Griditem>
    </Grid>
  </div>
);

export default Home;

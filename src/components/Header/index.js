import React from "react";
import styles from "./index.scss";

export function Header(props) {
  return <div {...props}>{props.children}</div>;
}

export default Header;

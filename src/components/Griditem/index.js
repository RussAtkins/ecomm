import React from "react";
import styles from "./index.scss";

export function Griditem(props) {
  return <div {...props}>{props.children}</div>;
}

export default Griditem;

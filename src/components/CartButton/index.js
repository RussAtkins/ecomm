import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

// action handlers
import { showCart, selector } from "../Cart/slice";

import styles from "./index.scss";

export function Template(props) {
  const CartButton = useSelector(selector);
  const dispatch = useDispatch();

  const toggleCart = () => {
    dispatch(showCart());
  };

  return (
    <button {...props} onClick={toggleCart}>
      Cart
    </button>
  );
}

export default Template;

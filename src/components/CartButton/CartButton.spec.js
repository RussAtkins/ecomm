context("Assertions", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/product");
    // Listen to GET to comments/1
    cy.intercept("GET", "//localhost:3001/product/*", {
      fixture: "product.json",
    });
  });

  describe("Toggle the cart", () => {
    it(".should() - show the cart ", () => {
      cy.get(".a-cart-button").click()
      cy.get(".m-cart").should('be.visible')  
    });
  });
});

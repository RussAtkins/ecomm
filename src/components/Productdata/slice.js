import { createSlice } from "@reduxjs/toolkit";

const productDataStructure = {
  product: {
    title: "",
    colours: [
      {
        color: "",
        image: "",
        defaultImage: true,
      },
      {
        color: "",
        image: "",
        defaultImage: false,
      },
    ],
  },
};

export const slice = createSlice({
  name: "ProductData",
  initialState: {
    value: productDataStructure,
    currentVariant: "",
    currentQuantity: "1",
  },
  reducers: {
    setCurrentProduct: (state, action) => {
      state.value = action.payload;
    },
    setCurrentVariant: (state, action) => {
      state.currentVariant = action.payload;
    },
    setCurrentQuantity: (state, action) => {
      state.currentQuantity = action.payload;
    },
  },
});

export const {
  setCurrentProduct,
  setCurrentVariant,
  setCurrentQuantity,
} = slice.actions;

export const ProductData = (state) => state.ProductData;

export default slice.reducer;

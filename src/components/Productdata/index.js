import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

// action handlers
import {
  setCurrentProduct,
  ProductData,
  setCurrentVariant,
  setCurrentQuantity,
} from "./slice";
import { showCart, addToCart } from "../Cart/slice";

// custom components
import Typography from "../Typography";
import Grid from "../Grid";
import Griditem from "../Griditem";
import ProductImg from "../ProductImg";

import styles from "./index.scss";

export function Template() {
  const dispatch = useDispatch();

  const product = useSelector(ProductData).value.product,
    productName = product.title,
    productVariants = product.colours,
    selectedVariant = useSelector(ProductData).currentVariant,
    selectedVariantImg = productVariants.filter((obj) => {
      return obj.color === selectedVariant;
    }),
    getSelectedVariantImg = () => {
      if (selectedVariantImg[0]) {
        return selectedVariantImg[0].image;
      }
    },
    selectedQuantity = useSelector(ProductData).currentQuantity;

  // get product data
  useEffect(() => {
    fetch("//localhost:3001/product/productid1")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        dispatch(setCurrentVariant(data.product.colours[0].color));
        dispatch(setCurrentProduct(data));
      });
  }, []);

  const handleVariantChange = (e) => {
    dispatch(setCurrentVariant(e.target.value));
  };

  const handleQuantityChange = (e) => {
    dispatch(setCurrentQuantity(e.target.value));
  };

  const handleAddToCart = () => {
    dispatch(showCart(true));
    dispatch(
      addToCart({
        id: productName,
        quantity: parseInt(selectedQuantity),
        variant: selectedVariant,
      })
    );
  };

  return (
    <Grid>
      <Griditem className="a-griditem--twothird">
        <ProductImg
          className="a-product-image"
          src={getSelectedVariantImg()}
          id="mainProductImage"
          alt={productName + " in " + selectedVariant}
        />
      </Griditem>
      <Griditem className="a-griditem--third">
        <h1 className="a-heading--h3">
          <Typography>{productName}</Typography>
        </h1>
        <label className="a-label" htmlFor="variant">
          <Typography>Choose Colour</Typography>
        </label>
        <select
          id="variant"
          value={selectedVariant}
          onChange={handleVariantChange}
          className="a-select"
        >
          {productVariants.map((variant, ind) => (
            <option key={ind} value={variant.color}>
              {variant.color}
            </option>
          ))}
        </select>
        <label className="a-label" htmlFor="quantity">
          <Typography>Quantity</Typography>
        </label>
        <input
          id="quantity"
          type="number"
          className="a-input"
          onChange={handleQuantityChange}
          defaultValue="1"
        />
        <button
          className="a-button--cta"
          id="addToCart"
          onClick={handleAddToCart}
        >
          <Typography>Add to cart</Typography>
        </button>
      </Griditem>
    </Grid>
  );
}

export default Template;

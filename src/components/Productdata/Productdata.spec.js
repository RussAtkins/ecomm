context("Assertions", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/product");
    // Listen to GET to comments/1
    cy.intercept("GET", "//localhost:3001/product/*", {
      fixture: "product.json",
    });
  });

  describe("Productdata should load", () => {
    it(".should() - have gotten the correct product name ", () => {
      cy.get("h1").contains("Nintendo Switch Console");
    });
  });

  describe("Switching products should", () => {
    it(".should() - switch variant value ", () => {
      cy.get("#variant").select("grey").should("have.value", "grey");
    });

    it(".should() - change the main image ", () => {
      cy.get("#mainProductImage").should(
        "have.attr",
        "src",
        "https://media.very.co.uk/i/very/QE9G4_SQ1_0000000054_CORAL_SLf?$550x733_standard$"
      );
      cy.get("#variant").select("grey");
      cy.get("#mainProductImage").should(
        "have.attr",
        "src",
        "https://media.very.co.uk/i/very/PEM4V_SQ1_0000000005_GREY_SLf?$550x733_standard$"
      );
    });
  });

  describe("Handle quantity changes", () => {
    it(".should() - change the value ", () => {
      cy.get("#quantity").clear().type("2").blur().should("have.value", "2");
      // cy.get('#quantity')
    });
  });

  describe("Handle adding to cart", () => {
    it(".should() - show the cart ", () => {
      cy.get("#addToCart").click();
      cy.get(".m-cart").should("have.class", "m-cart--visible");
    });

    it(".should() - have products in the cart ", () => {
      cy.get("#addToCart").click();
      cy.get(".m-cart").should("contain.text", "pink");
    });
  });

  describe("Handle adding multiple products to cart", () => {
    it(".should() - have 3 pink nintendos in the cart ", () => {
      cy.get("#addToCart").click();
      cy.get(".m-cart").should("contain.text", "1");
      cy.get(".m-cart").click()
      cy.get("#quantity").clear().type("2").blur().should("have.value", "2");
      cy.get("#addToCart").click();
      cy.get(".m-cart").should("contain.text", "3");
    });

    it(".should() - have 1 pink nintendo in the cart and 3 grey ", () => {
      cy.get("#addToCart").click();
      cy.get(".m-cart").click()
      cy.get("#variant").select("grey");
      cy.get("#quantity").clear().type("3").blur().should("have.value", "3");
      cy.get("#addToCart").click();
      cy.get(".m-cart").should("contain.text", "1");
      cy.get(".m-cart").should("contain.text", "3");
    });
  });
});

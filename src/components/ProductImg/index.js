import React from "react";
import styles from "./index.scss";

export function Griditem(props) {
  return (
    <div
      className="m-product-image"
      style={{ backgroundImage: `url(${props.src})` }}
    >
      <img {...props} />
    </div>
  );
}

export default Griditem;

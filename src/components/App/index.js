import React from "react";
import logo from "./logo.svg";
import { Template } from "../Template";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Template />
      </header>
    </div>
  );
}

export default App;

import React from "react";
import styles from "./index.scss";

export function Grid(props) {
  return <div className="a-grid">{props.children}</div>;
}

export default Grid;

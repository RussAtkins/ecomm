import React from "react";
import styles from "./index.scss";

export function Typography(props) {
  return (
    <span className="a-text" {...props}>
      {props.children}
    </span>
  );
}

export default Typography;

import { createSlice } from "@reduxjs/toolkit";

export const slice = createSlice({
  name: "Cart",
  initialState: {
    showCart: false,
    cartItems: {},
  },
  reducers: {
    showCart: (state, action) => {
      let value = action.payload;
      if (value === undefined) {
        value = !state.showCart;
      }
      state.showCart = value;
    },
    addToCart: (state, action) => {
      let cartItem = {
        [action.payload.id]: {
          [action.payload.variant]: action.payload.quantity,
        },
      };

      if (
        state.cartItems[action.payload.id] &&
        state.cartItems[action.payload.id][action.payload.variant]
      ) {
        state.cartItems[action.payload.id][action.payload.variant] += parseInt(
          action.payload.quantity
        );
      } else if (state.cartItems[action.payload.id]) {
        Object.assign(
          state.cartItems[action.payload.id],
          cartItem[action.payload.id]
        );
      } else {
        Object.assign(state.cartItems, cartItem);
      }
    },
  },
});

export const { showCart, addToCart } = slice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.template.value)`
export const selector = (state) => state.Cart;

export default slice.reducer;

import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import Typography from "../Typography";

// action handlers
import { showCart, selector } from "./slice";

import styles from "./index.scss";

export function Template() {
  const Cart = useSelector(selector);
  const dispatch = useDispatch();

  const closeCart = () => {
    dispatch(showCart(false));
  };

  return (
    <div
      className={`m-cart ${Cart.showCart && "m-cart--visible"}`}
      onClick={closeCart}
    >
      <div className="m-cart-content">
        <h3 className="a-heading--h3">
          <Typography>Your Cart</Typography>
        </h3>
        {/* {JSON.stringify(Cart.cartItems)} */}

        {Object.keys(Cart.cartItems).map((product, ind) => (
          <div key={product} className="a-cart-product">
            <h4>
              <Typography>{product}</Typography>
            </h4>
            {Object.keys(Cart.cartItems[product]).map((variant, ind) => (
              <div key={variant} className="a-cart-variant">
                <Typography>
                  - <strong>{variant}</strong> x{" "}
                  <strong>{Cart.cartItems[product][variant]}</strong>
                </Typography>
              </div>
            ))}
          </div>
        ))}
      </div>
      <button className="m-cart-closebutton" onClick={closeCart}>
        X
      </button>
    </div>
  );
}

export default Template;

context("Assertions", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/product");
    // Listen to GET to comments/1
    cy.intercept("GET", "//localhost:3001/product/*", {
      fixture: "product.json",
    });
  });

  describe("Toggle the cart", () => {
    it(".should() - show the cartnpm  ", () => {
      cy.get(".a-cart-button").click()
      cy.get(".m-cart").should('be.visible')  
      cy.get(".m-cart-closebutton").click()
      cy.get(".m-cart").should('not.be.visible')  
    });
  });
});

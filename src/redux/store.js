import { configureStore } from "@reduxjs/toolkit";
// import counterReducer from '../components/Counter/slice';
import productReducer from "../components/Productdata/slice";
import cartReducer from "../components/Cart/slice";

export default configureStore({
  reducer: {
    // counter: counterReducer,
    ProductData: productReducer,
    Cart: cartReducer,
  },
});

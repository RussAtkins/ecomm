context("Assertions", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/product");
    // Listen to GET to comments/1
    cy.intercept("GET", "//localhost:3001/product/*", {
      fixture: "product.json",
    });
  });

  describe("Toggle the cart", () => {
    it(".should() - change the value ", () => {
      cy.get("#quantity").clear().type("2").blur().should("have.value", "2");
      // cy.get('#quantity')
    });
  });
});

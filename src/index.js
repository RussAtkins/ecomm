import React from "react";
import ReactDOM from "react-dom";
import { useSelector, useDispatch } from "react-redux";
import store from "./redux/store";
import { Provider } from "react-redux";

// import * as serviceWorker from './serviceWorker';
import { Router, Link } from "@reach/router";

import { Helmet } from "react-helmet";

import "./themes/default/index.scss";

import Home from "./pageTemplates/home";
import Product from "./pageTemplates/product";

import Typography from "./components/Typography";
import Header from "./components/Header";
import Grid from "./components/Grid";
import Griditem from "./components/Griditem";
import Cart from "./components/Cart";
import CartButton from "./components/CartButton";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>My Title</title>
      </Helmet>
      <div className="main">
        <Header className="m-header">
          <Grid>
            <Griditem className="a-griditem--full m-header-content">
              <div className="m-header-nav">
                <Link to="/" className="m-header-logo">
                  <img src="//content.very.co.uk/assets/static/theme/images/material-design/logos/very-logo.svg" />
                </Link>
                <nav>
                  <Link to="/">
                    <Typography>Home</Typography>
                  </Link>{" "}
                  |{" "}
                  <Link to="product">
                    <Typography>Product</Typography>
                  </Link>
                </nav>
              </div>
              <CartButton className="a-cart-button" />
            </Griditem>
          </Grid>
        </Header>
        <Router className="content">
          <Home path="/" />
          <Product path="product" />
        </Router>
      </div>
      <Cart />
      <footer>
        <Grid>
          <Griditem className="a-griditem--full">
            <Typography>Footer design here</Typography>
          </Griditem>
        </Grid>
      </footer>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
